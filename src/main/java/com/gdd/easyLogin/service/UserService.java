package com.gdd.easyLogin.service;

import com.gdd.easyLogin.entity.User;

public interface UserService {

	boolean save(User user);

	boolean findUserByUsernameAndPassword(User user);

	boolean isRegister(User user);

}

package com.gdd.easyLogin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gdd.easyLogin.entity.User;
import com.gdd.easyLogin.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping("/login")
	public String login(User user){

		boolean success = userService.findUserByUsernameAndPassword(user);
		
		//登录成功
		if(success){
			return "login-success.html";			
		}else{
			return "redirect:/";
		}
	}

	@RequestMapping("/register")
	public String register(User user,String repassword,Model model){
//		System.out.println(user.getUsername());
//		System.out.println(user.getPassword());
//		System.out.println(repassword);
		//检验password以及repassword是否一致
		if(user.getPassword().equals(repassword)){
			//检验用户是否曾经注册过
			boolean isRegister = userService.isRegister(user);
			if(!isRegister){
				model.addAttribute("error",  "用户名已经存在！");
				return "error.html";
			}

			//一致	
			boolean success = userService.save(user);
			if(success){
				
				return "index.html";
			}else{
				model.addAttribute("error", "注册失败！");
				return "error.html";
			}
		}
		else{
			model.addAttribute("error", "两次密码输入不一致~");
			//不一致
			return "error.html";
		}	
	}

}
